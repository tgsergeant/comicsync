var base_url;
var current_page;
var machine_name;

function initRead(page, name, url) {
    current_page = page;
    base_url = url;
    machine_name = name;

    loadComic(page);
}

function loadComic(page) {
    var pagestr = page.toString();
    //Hackety hack to get Homestuck working
    if(machine_name == 'hs') {
        pagestr = "00" + pagestr;
    }
    $("#read-frame").attr('src', base_url + pagestr);
    $("#bar-text").text("Page " + page);

    current_page = page;
}

$("#previous-btn").click(previous);


$("#next-btn").click(next);

function syncAndLoad(new_page) {
    var csrftoken = getCookie('csrftoken');
    var b = {
        'page': new_page,
        'name':machine_name,
        'csrfmiddlewaretoken': csrftoken
    };
    $.post("/update/", b, function(data) {
        if(data != current_page.toString()) {
            $("#bar-text").text("Sync Error");
        }
    });
    loadComic(new_page);
}

// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


//Handle clicking the page number to jump
$("#bar-text").click(function() {
    $("#jump-form").removeClass('hide');
//    $("#bar-text").addClass("hide");
    $("#jump-field").val(current_page);
    $("#jump-field").select();
});

$("#jump-form").submit(function() {
    $("#jump-form").addClass("hide");
//    $("#bar-text").removeClass("hide");

    console.log($("#jump-field").val());

    var enteredNumber = parseInt($("#jump-field").val());
    console.log(enteredNumber);

    if (enteredNumber > 0) {
        syncAndLoad(enteredNumber);
    }

    return false;
});

function next() {
    syncAndLoad(current_page + 1);
    return false;
}

function previous() {
    syncAndLoad(current_page - 1);
    return false;
}
$(document).bind('keypress', 'right', next);
$(document).bind('keypress', 'left', previous)