from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'ComicSync.comic.views.home', name='home'),
    url(r'^read/(?P<name>.+)/$', 'ComicSync.comic.views.read', name='read'),
    url(r'^update/', 'ComicSync.comic.views.update', name='update'),
    # url(r'^ComicSync/', include('ComicSync.foo.urls')),
    url(r'^browserid/', include('django_browserid.urls')),
    url(r'^logout/', 'django.contrib.auth.views.logout', name="logout", kwargs={'next_page':'/'}),


    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
