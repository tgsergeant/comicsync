from django.contrib.auth.models import User
from django.db import models

class Comic(models.Model):
    machine_name = models.CharField(max_length=15, primary_key=True)
    name = models.CharField(max_length=100)
    homepage = models.URLField()
    comic_base_url = models.URLField()
    first_page_number = models.IntegerField()
    image_url = models.CharField(max_length=150)
    description = models.TextField()

    def __unicode__(self):
        return self.name

class SyncLocation(models.Model):
    class Meta:
        unique_together = ('user', 'comic')

    user = models.ForeignKey(User)
    comic = models.ForeignKey(Comic)
    page = models.IntegerField()
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "%d of %s" % (self.page, self.comic.name)