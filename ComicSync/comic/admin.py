from django.contrib import admin
from ComicSync.comic.models import SyncLocation, Comic

class ComicAdmin(admin.ModelAdmin):
    list_display = ('machine_name', 'name', 'homepage')

admin.site.register(SyncLocation)

admin.site.register(Comic, ComicAdmin)