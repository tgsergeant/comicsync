# Create your views here.
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render_to_response, get_object_or_404
from django.template.context import RequestContext
from ComicSync.comic.models import SyncLocation, Comic

def home(request):
    if request.user.is_authenticated():
        #Logged in user
        syncs = request.user.synclocation_set.order_by('-updated')

        unread_comics = Comic.objects.exclude(synclocation__user=request.user)

        return render_to_response('user_index.html', {'syncs':syncs, 'unread':unread_comics}, context_instance=RequestContext(request))
    else:
        comics = Comic.objects.all()
        return render_to_response('home.html', {'comics': comics}, context_instance=RequestContext(request))


@login_required
def read(request, name):
    #Find the comic
    comic = get_object_or_404(Comic, machine_name=name)
    first_visit = False

    #Retrieve the user's current page
    try:
        location = request.user.synclocation_set.get(comic = comic)
    except SyncLocation.DoesNotExist:
        #See if this is their first visit
        if not request.user.synclocation_set.count():
            first_visit = True
        location = SyncLocation.objects.create(user = request.user, page = comic.first_page_number, comic = comic)

    options = {
        'page': location.page,
        'comic': comic,
        'first':first_visit,
    }
    return render_to_response('read.html', options, context_instance=RequestContext(request))

@login_required
def update(request):
    if request.method == 'POST':
        page = request.POST['page']
        machine_name = request.POST['name']

        sync = request.user.synclocation_set.get(comic__pk = machine_name)
        sync.page = page
        sync.save()
        return HttpResponse(page)
